First, revert upstream changes since 4.2

diff --git a/examples/chrony.nm-dispatcher.dhcp b/examples/chrony.nm-dispatcher.dhcp
index 547ce83f..6ea4c370 100644
--- a/examples/chrony.nm-dispatcher.dhcp
+++ b/examples/chrony.nm-dispatcher.dhcp
@@ -1,7 +1,8 @@
 #!/bin/sh
 # This is a NetworkManager dispatcher script for chronyd to update
-# its NTP sources with servers from DHCP options passed by NetworkManager
-# in the DHCP4_NTP_SERVERS and DHCP6_DHCP6_NTP_SERVERS environment variables.
+# its NTP sources passed from DHCP options. Note that this script is
+# specific to NetworkManager-dispatcher due to use of the
+# DHCP4_NTP_SERVERS environment variable.
 
 export LC_ALL=C
 
@@ -9,23 +10,17 @@ interface=$1
 action=$2
 
 chronyc=/usr/bin/chronyc
-server_options=iburst
+default_server_options=iburst
 server_dir=/var/run/chrony-dhcp
 
 dhcp_server_file=$server_dir/$interface.sources
-dhcp_ntp_servers="$DHCP4_NTP_SERVERS $DHCP6_DHCP6_NTP_SERVERS"
+# DHCP4_NTP_SERVERS is passed from DHCP options by NetworkManager.
+nm_dhcp_servers=$DHCP4_NTP_SERVERS
 
 add_servers_from_dhcp() {
     rm -f "$dhcp_server_file"
-    for server in $dhcp_ntp_servers; do
-        # Check for invalid characters (from the DHCPv6 NTP FQDN suboption)
-        len1=$(printf '%s' "$server" | wc -c)
-        len2=$(printf '%s' "$server" | tr -d -c 'A-Za-z0-9:.-' | wc -c)
-        if [ "$len1" -ne "$len2" ] || [ "$len2" -lt 1 ] || [ "$len2" -gt 255 ]; then
-          continue
-        fi
-
-        printf 'server %s %s\n' "$server" "$server_options" >> "$dhcp_server_file"
+    for server in $nm_dhcp_servers; do
+        echo "server $server $default_server_options" >> "$dhcp_server_file"
     done
     $chronyc reload sources > /dev/null 2>&1 || :
 }
@@ -39,11 +34,10 @@ clear_servers_from_dhcp() {
 
 mkdir -p $server_dir
 
-case "$action" in
-    up|dhcp4-change|dhcp6-change)
-        add_servers_from_dhcp;;
-    down)
-        clear_servers_from_dhcp;;
-esac
+if [ "$action" = "up" ] || [ "$action" = "dhcp4-change" ]; then
+    add_servers_from_dhcp
+elif [ "$action" = "down" ]; then
+    clear_servers_from_dhcp
+fi
 
 exit 0

From: Robert Fairley <rfairley@redhat.com>
Date: Wed, 17 Jun 2020 10:14:19 -0400
Subject: [PATCH] examples/nm-dispatcher.dhcp: use sysconfig

Use the PEERNTP and NTPSERVERARGS environment variables from
/etc/sysconfig/network{-scripts}.

Co-Authored-By: Christian Glombek <cglombek@redhat.com>

diff --git a/examples/chrony.nm-dispatcher.dhcp b/examples/chrony.nm-dispatcher.dhcp
index 6ea4c37..a6ad35a 100644
--- a/examples/chrony.nm-dispatcher.dhcp
+++ b/examples/chrony.nm-dispatcher.dhcp
@@ -6,16 +6,24 @@
 
 chronyc=/usr/bin/chronyc
 default_server_options=iburst
-server_dir=/var/run/chrony-dhcp
+server_dir=/run/chrony-dhcp
 
 dhcp_server_file=$server_dir/$interface.sources
 # DHCP4_NTP_SERVERS is passed from DHCP options by NetworkManager.
 nm_dhcp_servers=$DHCP4_NTP_SERVERS
 
+[ -f /etc/sysconfig/network ] && . /etc/sysconfig/network
+[ -f /etc/sysconfig/network-scripts/ifcfg-"${interface}" ] && \
+    . /etc/sysconfig/network-scripts/ifcfg-"${interface}"
+
 add_servers_from_dhcp() {
     rm -f "$dhcp_server_file"
+
+    # Don't add NTP servers if PEERNTP=no specified; return early.
+    [ "$PEERNTP" = "no" ] && return
+
     for server in $nm_dhcp_servers; do
-        echo "server $server $default_server_options" >> "$dhcp_server_file"
+        echo "server $server ${NTPSERVERARGS:-$default_server_options}" >> "$dhcp_server_file"
     done
     $chronyc reload sources > /dev/null 2>&1 || :
 }
-- 
2.29.2

Use chrony-helper instead of chronyc to avoid changes in default chrony.conf

diff -up chrony-4.1/examples/chrony.nm-dispatcher.dhcp.nm-dispatcher-dhcp chrony-4.1/examples/chrony.nm-dispatcher.dhcp
--- chrony-4.1/examples/chrony.nm-dispatcher.dhcp.nm-dispatcher-dhcp	2021-06-09 11:10:30.997416152 +0200
+++ chrony-4.1/examples/chrony.nm-dispatcher.dhcp	2021-06-09 11:16:23.598381336 +0200
@@ -9,11 +9,12 @@ export LC_ALL=C
 interface=$1
 action=$2
 
-chronyc=/usr/bin/chronyc
+helper=/usr/libexec/chrony-helper
 default_server_options=iburst
-server_dir=/run/chrony-dhcp
+server_dir=/run/chrony-helper
 
-dhcp_server_file=$server_dir/$interface.sources
+dhcp_server_tmpfile=$server_dir/tmp-nm-dhcp.$interface
+dhcp_server_file=$server_dir/nm-dhcp.$interface
 # DHCP4_NTP_SERVERS is passed from DHCP options by NetworkManager.
 nm_dhcp_servers=$DHCP4_NTP_SERVERS
 
@@ -24,24 +24,30 @@ nm_dhcp_servers=$DHCP4_NTP_SERVERS
 add_servers_from_dhcp() {
     rm -f "$dhcp_server_file"
 
+    # Remove servers saved by the dhclient script before it detected NM.
+    rm -f "/var/lib/dhclient/chrony.servers.$interface"
+
     # Don't add NTP servers if PEERNTP=no specified; return early.
     [ "$PEERNTP" = "no" ] && return
 
+    # Create the directory with correct SELinux context.
+    $helper create-helper-directory > /dev/null 2>&1
+
     for server in $nm_dhcp_servers; do
-        echo "server $server ${NTPSERVERARGS:-$default_server_options}" >> "$dhcp_server_file"
+        echo "$server ${NTPSERVERARGS:-$default_server_options}" >> "$dhcp_server_tmpfile"
     done
+    [ -e "$dhcp_server_tmpfile" ] && mv "$dhcp_server_tmpfile" "$dhcp_server_file"
-    $chronyc reload sources > /dev/null 2>&1 || :
+
+    $helper update-daemon > /dev/null 2>&1 || :
 }
 
 clear_servers_from_dhcp() {
     if [ -f "$dhcp_server_file" ]; then
         rm -f "$dhcp_server_file"
-        $chronyc reload sources > /dev/null 2>&1 || :
+        $helper update-daemon > /dev/null 2>&1 || :
     fi
 }
 
-mkdir -p $server_dir
-
 if [ "$action" = "up" ] || [ "$action" = "dhcp4-change" ]; then
     add_servers_from_dhcp
 elif [ "$action" = "down" ]; then
